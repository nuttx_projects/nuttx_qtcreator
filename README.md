# NuttX QtCreator file templates

This package includes source file templates for QtProject, which include the appropriate
comment sections following NuttX style. Also, since QtCreator also allows specifying a
template license for new files, this package also includes a BSD license template following
NuttX style.

## Installation and use

QtProject templates can be installed inside `$HOME/.config/QtProject/qtcreator/templates/wizards/` (create the required subdirectories, if they do not exist). Inside this directory you can clone this repository or place a symlink to it. Restart QtCreator and they will appear when choosing "New File"

To select the license template, go to `Tools -> Options -> C++ -> File Naming` and choose the path to the template. This template will expects your full name and e-mail to be supplied via environment. An easy way to define this is to add the following lines to your `.bashrc`:

    export GIT_FULL_NAME=$(git config --global user.name)
    export GIT_EMAIL=$(git config --global user.email)
 
Which gets this information from your global git settings. In order for this to work you need to either start qtcreator from a console or copy the corresponding .desktop file and modify the `Exec` to add `bash -l -c` before the path to the qtcreator binary. 

## Tip

On QtCreator you will probably use the "Generic Project". This auto-generates the include paths from the files included to the project itself. This is not current since will include many unnecessary and sometimes conflicting include paths for the auto-complete to work correctly. To fix this, go into the .includes file and remove all entries for nuttx/ and apps/ directories and only leave this:

apps/include
nuttx/include
nuttx/fs

You may have to add some other paths but this will get you quite close of how it should work.
