%{Cpp:LicenseTemplate}\

#ifndef __%{JS: Cpp.headerGuard('%{FileName}')}__
#define __%{JS: Cpp.headerGuard('%{FileName}')}__

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/input/ioctl.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Type Definitions
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Name: %{DriverName}_register
 *
 * Description:
 *   Register the %{DriverName} character driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the %{DriverName} device to be registered.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int %{DriverName}_register(FAR const char *devname);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif // __%{JS: Cpp.headerGuard('%{FileName}')}__
