%{Cpp:LicenseTemplate}\
		
/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdbool.h>
#include <string.h>
#include <poll.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/signal.h>
#include <nuttx/fs/fs.h>

#include <nuttx/irq.h>

/****************************************************************************
 * Private Types
 ****************************************************************************/

/* This structure provides the state of the %{DriverName} driver */

struct %{DriverName}_dev_s
{
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     %{DriverName}_open(FAR struct file *filep);
static int     %{DriverName}_close(FAR struct file *filep);
@if %{implementReadOp} 
static ssize_t %{DriverName}_read(FAR struct file *filep, FAR char *buffer,
                         size_t buflen);
@endif
@if %{implementWriteOp}
static ssize_t %{DriverName}_read(FAR struct file *filep, FAR const char *buffer,
                         size_t buflen);
@endif
@if %{implementSeekOp}
static off_t   %{DriverName}_seek(FAR struct file *filep, off_t offset,
                                int whence);
@endif
@if %{implementPollOp}
static int     %{DriverName}_poll(FAR struct file *filep, struct pollfd *fds,
                              bool setup);
@endif
@if %{implementUnlinkOp}
static int     %{DriverName}_unlink(FAR struct inode *inode);
@endif
@if %{implementIoctlOp}
static int     %{DriverName}_ioctl(FAR struct file *filep, int cmd,
                          unsigned long arg);
@endif

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations %{DriverName}_fops =
{
  %{DriverName}_open,  /* open */
  %{DriverName}_close, /* close */
@if %{implementReadOp}
  %{DriverName}_read,  /* read */
@else
  NULL,		       /* read */
@endif
@if %{implementWriteOp}
  %{DriverName}_write, /* write */
@else
  NULL,		       /* write */
@endif
@if %{implementSeekOp}
  %{DriverName}_seek,  /* seek */
@else
  NULL,                /* seek */
@endif
@if %{implementIoctlOp}
  %{DriverName}_ioctl, /* ioctl */
@else
  NULL,                /* ioctl */
@endif
@if %{implementPollOp}
  %{DriverName}_poll   /* poll */
@else
  NULL                 /* poll */
@endif
#ifndef CONFIG_DISABLE_PSEUDOFS_OPERATIONS
@if %{implementUnlinkOp}
  , %{DriverName}_unlink /* unlink */
@else
  , NULL                 /* unlink */
@endif
#endif
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: %{DriverName}_open
 ****************************************************************************/

static int %{DriverName}_open(FAR struct file *filep)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return ret;
}

/****************************************************************************
 * Name: %{DriverName}_close
 ****************************************************************************/

static int %{DriverName}_close(FAR struct file *filep)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return ret;
}
@if %{implementReadOp}

/****************************************************************************
 * Name: %{DriverName}_read
 ****************************************************************************/

static ssize_t %{DriverName}_read(FAR struct file *filep, FAR char *buffer,
                         size_t len)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return (ssize_t)ret;
}
@endif
@if %{implementWriteOp}

/****************************************************************************
 * Name: %{DriverName}_write
 ****************************************************************************/

static ssize_t %{DriverName}_write(FAR struct file *filep,
				   FAR const char *buffer, size_t len)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return (ssize_t)ret;
}
@endif
@if %{implementSeekOp}

/****************************************************************************
 * Name: %{DriverName}_seek
 ****************************************************************************/

static off_t %{DriverName}_seek(FAR struct file *filep, off_t offset,
				int whence)
{
  FAR struct %{DriverName}_dev_s *priv;
  off_t ret = 0;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return ret;
}
@endif
@if %{implementPollOp}

/****************************************************************************
 * Name: %{DriverName}_poll
 ****************************************************************************/
static int %{DriverName}_poll(FAR struct file *filep, struct pollfd *fds,
			      bool setup)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return ret;	
}
@endif
@if %{implementUnlinkOp}

/****************************************************************************
 * Name: %{DriverName}_unlink
 ****************************************************************************/
  
static int %{DriverName}_unlink(FAR struct inode *inode)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  return ret;
}
@endif
@if %{implementIoctlOp}

/****************************************************************************
 * Name: %{DriverName}_ioctl
 ****************************************************************************/

static int %{DriverName}_ioctl(FAR struct file *filep, int cmd, unsigned long arg)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  priv  = (FAR struct %{DriverName}_dev_s *)filep->f_inode->i_private;

  switch(cmd)
  {
    default:
      ret = -EINVAL;
    break;
  }

  return ret;
}
@endif

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: %{DriverName}_register
 *
 * Description:
 *   Register the %{DriverName} driver as the specified device.
 *
 * Input Parameters:
 *   devname - The name of the %{DriverName} device to be registered.
 *
 * Returned Value:
 *   Zero (OK) is returned on success.  Otherwise a negated errno value is
 *   returned to indicate the nature of the failure.
 *
 ****************************************************************************/

int %{DriverName}_register(FAR const char *devname)
{
  FAR struct %{DriverName}_dev_s *priv;
  int ret = OK;

  DEBUGASSERT(devname);

  /* Allocate a new %{DriverName} driver instance */

  priv = (FAR struct %{DriverName}_dev_s *)kmm_zalloc(sizeof(struct %{DriverName}_dev_s));

  ret = register_driver(devname, &%{DriverName}_fops, 0666, priv);

  if (ret < 0)
  {
	  kmm_free(priv);
	  ret = ERROR;
  }

  return ret;
}
